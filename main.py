import os
import random
import requests

from flask import Flask, render_template, request, redirect, url_for
from utils import process_lena, get_envs

app = Flask(__name__)

@app.route("/")
def home():
    """ Home page."""
    
    app.logger.info("Lena Image Processing App")
    
    return render_template("index.html")

@app.route("/apply-noise", methods=["POST"])
def choose_noise():
    """ Get noise type. """
    
    noise = request.form.get('noise')
    app.logger.info(f'Selected Noise: {noise}')
    
    img_name = process_lena(noise_type=noise)
    img_path = url_for('static', filename=f'images/{img_name}')
    
    app.logger.info(img_path)
    app.logger.info("Generated!")
    
    return render_template("lena_processed.html", noise=noise, img_path=f'{os.environ["AUTH_URL_PREFIX"]}/{os.environ["BUCKET_NAME"]}/{img_name}' if int(os.environ["K8S_CLUSTER"]) else img_path)

@app.route("/back-home", methods=["POST"])
def back_home():
    """ Back to home page."""
    
    app.logger.info("Going to home page...")

    return redirect(url_for("home"))

@app.route("/get-random-number")
def get_random_number():
    random_number = random.randint(0, 100)

    return f"The Random Number is: {random_number}"

@app.route("/get-number")
def get_number():

    # Get type of numbers to generate. Valid options are: "even" or "odd".
    number_type = os.environ["NUMBERS_TYPE"]
    start = 0 if number_type == "even" else 1
    number = random.randrange(start, 100, 2)

    return f"The Random {number_type} Number is: {number}"

@app.route("/prime")
def get_prime_number():
    cloud_function_uri = os.environ["CLOUDFUNCTION_URI"]
    response = requests.get(cloud_function_uri).text

    return response

@app.route("/k8s-info")
def get_k8s_info():
    # Name of environment variable under which should be stored info about k8s cluster.
    pod, node = "POD_NAME", "NODE_NAME"
    
    # Message in case of not specified info about k8s cluster which means that probably app is not running on k8s cluster.
    no_k8s_info = "Are you sure you are running your application in k8s cluster?"
    
    pod_info, node_info = get_envs(envs=[pod, node], msg=no_k8s_info)

    return f"Pod Name: {pod_info} Node Name: {node_info}"

@app.route("/author")
def get_author_info():
    # Name of environment variable under which should be stored info about author.
    name, surname = "NAME", "SURNAME"
    
    # Message in case of not specified info about author.    
    no_author_info = "Not given"

    name_info, surname_info = get_envs(envs=[name, surname], msg=no_author_info)

    return f"Name: {name_info} Surname: {surname_info}"

@app.route("/error")
def get_error():
    error_var = "intended error here!"

    return error_varr

@app.route("/new-feature")
def new_feat():
    return "New feature!!!"

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
