import os
import imgaug.augmenters as iaa
import numpy as np
import random

from datetime import datetime
from pathlib import Path
from PIL import Image

from google.cloud import storage

LENA_DIR = Path.cwd().joinpath('static', 'images')
DEFAULT_NOISE_AMOUNT = 0.3

def get_envs(envs, msg):
    env_list = []
    for env in envs:
        try:
            env_name = os.environ[f"{env}"]
        except KeyError:
            env_name = msg
        
        env_list.append(env_name)

    return env_list

def get_noise_amount(noise_type):
    # Change name of 'Salt and Pepper' noise to match env variable.
    noise_type = "sap" if noise_type == "Salt and Pepper" else noise_type

    try:
        noise_amount_min = float(os.environ[f"{noise_type.upper()}_NOISE_AMOUNT_MIN"])
    except KeyError:
        noise_amount_min = "default"

    try:
        noise_amount_max = float(os.environ[f"{noise_type.upper()}_NOISE_AMOUNT_MAX"])
    except KeyError:
        noise_amount_max = "default"

    # Get random noise amount from range given in env variables. If not passed, get the default value.
    noise_amount = random.uniform(noise_amount_min, noise_amount_max) if (noise_amount_min != "default " and noise_amount_max != "default") else DEFAULT_NOISE_AMOUNT

    return noise_amount

def upload_image(img_path):
    storage_client = storage.Client()
    bucket_name = os.environ["BUCKET_NAME"]
    bucket = storage_client.bucket(bucket_name)

    if not bucket.exists():
        bucket.create()

    blob = bucket.blob(img_path.name)
    blob.upload_from_filename(img_path)

def process_lena(noise_type):
    noise_amount = get_noise_amount(noise_type)
    if noise_type == 'Gaussian':
        noise = iaa.AdditiveGaussianNoise(loc=0, scale=(0, noise_amount*255))
    elif noise_type == 'Salt and Pepper':
        noise = iaa.SaltAndPepper(noise_amount)
    elif noise_type == 'Brightness':
        noise = iaa.pillike.EnhanceBrightness(factor=noise_amount)

    img = np.array(Image.open(LENA_DIR.joinpath('lena.png').as_posix()), dtype=np.uint8)
    img = noise(image=img)
    img = Image.fromarray(img)
    
    date = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
    img_name = f"lena_{noise_type.replace(' ', '').lower()}_modified_{date}.png"
    img_path = LENA_DIR.joinpath(img_name)
    img.save(img_path)

    # Upload processed Lena to the GCP Bucket. Check if application is running on k8s cluster. Mapping to int because env variable is set to 0 (False) or 1 (True).
    if int(os.environ["K8S_CLUSTER"]):
        upload_image(img_path)
    
    return img_name

