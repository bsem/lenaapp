FROM python:3.8-slim-buster

RUN apt update && apt install ffmpeg libsm6 libxext6 -y

WORKDIR /app

COPY . /app

RUN pip3 install -r requirements.txt

CMD ["python3", "main.py"]

